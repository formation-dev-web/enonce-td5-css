TD5 CSS − Flexbox
=================

Partir du corrigé du td3 (contenu dans ce dépôt) comme base. Nous allons
utiliser flexbox pour garder la même apparence visuelle tout en simplifiant
certaines parties de la CSS.


1. Utiliser `display: flex` sur le `<ul>` du premier `<nav>` pour positionner
  ses `<li>` sans recours à `inline-block`.

2. Utiliser `display: flex` sur le `<div>` pour enlever le `position: absolute`
  du `<main>` et de l'`<aside>` et placer ces deux colonnes avec flexbox.
  (laisser pour l'instant les `width` de chaque colonne). Pensez à restaurer
  l'espace entre les deux colonnes.

3. Nous allons fixer la taille de la colonne de droite, et laisser la colonne
   de gauche s'adapter à l'espace disponible :
  - Fixer la largeur de la colonne de droite en *rem*, et l'empêcher de
    grandir/rétrécir (cf `flex-shrink` `flex-grow`).
  - Enlever le `width` de la colonne de gauche, afin qu'elle récupère tout
    l'espace laissé par la colonne de droite.

4. Utiliser `display: flex` sur le `<header>` afin d'enlever le `position
  absolute` du `<p>` qu'il contient (il faut jouer sur la manière dont est
  distribué l'espace entre les items).

5. Utiliser les flexbox pour positionner les éléments du nav du bas sans
   `inline-block` et sans `width`
